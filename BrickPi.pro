TEMPLATE = subdirs

SUBDIRS += \
    driver \
    test \
    explorer

diver.file = driver/driver.pro
test.file = test/test.pro
test.depends = driver
explorer.file = explorer/explorer.pro
explorer.depends = driver

OTHER_FILES = README.md
