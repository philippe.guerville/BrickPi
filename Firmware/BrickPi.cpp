/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#include "BrickPi.h"
#include "UART.h"
#include "Motor.h"
#include "Sensor.h"
#include "Message.h"

BrickPi brickpi;

BrickPi::BrickPi()
{
  m_uart = new UART();
  for (int port = 0; port < 2; port++) {
    m_motors[port] = new Motor(port);
    m_sensors[port] = new Sensor(port);
  }
  m_timeout = 0;
}

BrickPi::~BrickPi()
{
  delete m_uart;
  for (int port = 0; port < 2; port++) {
    delete m_motors[port];
    delete m_sensors[port];
  }
}

void BrickPi::setup()
{
  //set timer1 interrupt at 10Hz
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 10hz increments
  OCR1A = 1562;// = (16*10^6) / (10*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS10 and CS12 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  //allow interrupts
  sei();
  
  m_uart->setup();
  Motor::setup();
}

void BrickPi::update()
{
  Message request;
  
  if (m_uart->receive(&request)) {
    // shutdown timeout
    m_timeout = millis() + 250;
    switch(request.type) {
      case Message::MOTOR0:
        update(request, m_motors[M0]);
      break;
      case Message::MOTOR1:
        update(request, m_motors[M1]);
      break;
      case Message::SENSOR0:
        update(request, m_sensors[S0]);
      break;
      case Message::SENSOR1:
        update(request, m_sensors[S1]);
      break;
    }
  }

  if (millis() > m_timeout) {
    // timeout -> stop motors and sensors
    Motor::stop();
    m_sensors[S0]->reset();
    m_sensors[S1]->reset();
  }
}

void BrickPi::keepalive()
{
  // called by timer1 interrupt to keep alive ev3 sensors
  for (int port = 0; port < 2; port++) {
    m_sensors[port]->keepalive();
  }
}

void BrickPi::setTimeout(unsigned long timeout)
{
  // called by ev3 sensors to set a longer timeout during initialisation
  m_timeout = timeout;
}

void BrickPi::debug(const char* fmt, ...)
{ 
  // helper function that send a debug message 
  Message answer(Message::DBG);
  va_list ap;
  va_start(ap, fmt);
  answer.length = vsnprintf(answer.data, 255, fmt, ap);
  va_end(ap);
  m_uart->send(answer);
}

void BrickPi::update(const Message& request, Motor* motor)
{
  // set motor's power according to request
  uint8_t dir = (uint8_t)request.data[0];
  uint8_t pow = (uint8_t)request.data[1];
  motor->power(dir, pow);
  
  // provide encoder value in the answer
  Message answer(request.type);
  answer.length = sizeof(uint32_t);
  int32_t* ptr = (int32_t*)answer.data;
  *ptr = motor->encoder();
  m_uart->send(answer);
}

void BrickPi::update(const Message& request, Sensor* sensor)
{
  // set sensor mode according to request
  byte mode = (byte)request.data[0];
  sensor->update(mode);

  // provide current sensor id (giro, us, color, ...)
  // provide current mode, it can take many iteration to switch the sensor from one mode to another one
  // provide current sensor raw value
  Message answer(request.type);
  answer.length = 4;
  answer.data[0] = sensor->id();
  answer.data[1] = sensor->mode();
  int16_t* ptr = (int16_t*)(answer.data + 2);
  *ptr = sensor->value();
  m_uart->send(answer);
}

ISR(TIMER1_COMPA_vect)
{
  // called by interruption every 100 ms
  brickpi.keepalive();
}

uint8_t XOR(const void* data, size_t size)
{
  // use exclusive or as checksum algorithm
  if (size <= 1) return 0;

  uint8_t chksum = 0xFF;
  const uint8_t* ptr = (const uint8_t*)data;
  const uint8_t* end = ptr + size;

  while (ptr < end) chksum ^= *ptr++;

  return chksum;
}

const char *byte_to_binary(byte x)
{
  // helper function that return the binary representation of a byte
  static char b[9];
  b[8] = '\0';
  char* ptr = b;

  byte z;
  for (z = 128; z > 0; z >>= 1) {
    *ptr++ = ((x & z) == z) ? '1' : '0';
  }

  return b;
}

