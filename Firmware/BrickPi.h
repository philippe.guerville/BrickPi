/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#ifndef BRICKPI_H
#define BRICKPI_H

#include "Arduino.h"
#include <stdint.h>
#include <stdlib.h>

enum Port
{
    M0 = 0,
    M1 = 1,
    S0 = 0,
    S1 = 1
};

class UART;
class Motor;
class Sensor;
class Message;

class BrickPi
{
public:
  BrickPi();
  ~BrickPi();

  void setup();
  void update();
  void keepalive();
  void setTimeout(unsigned long timeout);
  void debug(const char* fmt, ...);
  
private:
  void update(const Message& request, Motor* motor);
  void update(const Message& request, Sensor* sensor);

  UART* m_uart;
  Motor* m_motors[2];
  Sensor* m_sensors[2];
  volatile unsigned long m_timeout;
};

extern BrickPi brickpi;

uint8_t XOR(const void* data, size_t size);
const char *byte_to_binary(byte x);

#endif

