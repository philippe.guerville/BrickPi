/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#include "Message.h"
#include "BrickPi.h"
#include <string.h>

Header::Header(uint8_t recipient, uint8_t type, uint8_t length):
  recipient(recipient),
  type(type),
  length(length),
  chksum(0)
{
}

void Header::reset() 
{
  memset(this, 0, sizeof(Header));
}

Message::Message(uint8_t type):
  Header(0, type, 0)
{
}

void Message::finalize()
{
  chksum = 0;
  chksum = XOR(this, sizeof(Header) + length);
}

bool Message::validate() const
{
  return XOR(this, sizeof(Header) + length) == 0;
}

