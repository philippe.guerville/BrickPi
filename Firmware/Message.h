/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include <stdint.h>

struct Header
{
  Header(uint8_t recipient, uint8_t type, uint8_t length);

  void reset();
  
  uint8_t recipient;    // MCU1 or MCU2
  uint8_t type;         // DBG, MOTOR0 or 1, SENSOR0 or 1
  uint8_t length;       // length of the data array
  uint8_t chksum;       // checksum, xor algorithm
};

struct Message : public Header
{
  Message(uint8_t type = VOID);
 
  enum Type {
    VOID,
    DBG,
    MOTOR0,
    MOTOR1,
    SENSOR0,
    SENSOR1
  };

  void finalize();
  bool validate() const;

  char data[256];
};

#endif // MESSAGE_H

