/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#include "Motor.h"
#include "BrickPi.h"

// pin 10 controls motor0 power
// pin 8 and 12 control motor0 direction
// pin 11 controls motor1 power
// pins 13 and 9 control motor1 direction
uint8_t Motor::s_pins[][3] = {{8, 12, 10}, {13, 9, 11}};

// encoder values of motor0 and 1
volatile int32_t Motor::s_enc[2] = {0, 0};

// encoder state (last end current) of motor0 and 1
uint8_t Motor::s_state[2] = {0, 0};

Motor::Motor(uint8_t port):
  m_port(port)
{
}

void Motor::setup()
{
  // motor0 encoder use pins 2 and 4
  // motor1 encoder use pins 3 and 5
  // setup the encoder pins as inputs, with pullups disabled.
  for (int pin = 2; pin <= 5; pin++) {
    pinMode(pin, INPUT);
  }
  // Enable the PCINT channels for reading the encoders.
  PCMSK2 |= 0x3C;        // React to PCINT 18 to 21 (pins 2 to 5)
  PCICR |= 0x04;         // Enable PCINT2

  stop();

  // set pins 8 to 13 as output
  for (int port = 0; port < 2; port++) {
    for (int pin = 0; pin < 3; pin++) {
      pinMode(s_pins[port][pin], OUTPUT);
    }
  }
}

void Motor::stop()
{
  // Set all pins to LOW
  for (int port = 0; port < 2; port++) {
    for (int pin = 0; pin < 3; pin++) {
      digitalWrite(s_pins[port][pin], LOW);
    }
  }
  // reset encoder values
  s_enc[M0] = s_enc[M1] = 0;
}

void Motor::power(uint8_t dir, uint8_t pow)
{ 
  pow = (dir != Float && dir != Locked) ? pow : 0;

  // If there's a change of direction, we first set the motor down to 0
  pow = (dir == m_lastDir || m_lastDir == Float || m_lastDir == Locked) ? pow : 0;
  m_lastDir = dir;

  switch (dir) {
  case Float:
    digitalWrite(s_pins[m_port][0], LOW);
    digitalWrite(s_pins[m_port][1], LOW);
    break;
  case Locked:
    digitalWrite(s_pins[m_port][0], HIGH);
    digitalWrite(s_pins[m_port][1], HIGH);
    break;
  case CW:
    digitalWrite(s_pins[m_port][0], HIGH);
    digitalWrite(s_pins[m_port][1], LOW);
    break;
  case CCW:
    digitalWrite(s_pins[m_port][0], LOW);
    digitalWrite(s_pins[m_port][1], HIGH);
    break;
  }
  analogWrite(s_pins[m_port][2], pow);
}

int32_t Motor::encoder() const
{
  // critical section, encoder value are updated by interruption
  noInterrupts();
  int32_t val = s_enc[m_port];
  interrupts();
  return val;
}

/**
 *  for each encoder, (pins 2,4 or pins 3,5) the change sequence indicate
 *  if the motor is rotating clockwise or counterclockwise
 *
 *   CW | CCW
 *  ----------
 *  0 0 | 0 0
 *  1 0 | 0 1
 *  1 1 | 1 1
 *  0 1 | 1 0
 *  0 0 | 0 0
 *
 *  the previous and current state of each encoder is store in s_states[]
 *  bits 0 and 1 contain the current state
 *  bits 2 and 3 contain the previous state
 *  lookup table state indicate if we are rotating CW (+1) or CCW (-1)
 *  if previous state == current state the motor is stopped
 *  if we missed a transition (e.g. 00 -> 11) it's not possible to determine the rotation direction 
 */

void Motor::update(uint8_t port)
{
  //                              0000 0001 0010 0011 0100 0101 0110 0111 1000 1001 1010 1011 1100 1101 1110 1111
  static const int8_t states[] = {   0,  -1,   1,   0,   1,   0,   0,  -1,  -1,   0,   0,   1,   0,   1,  -1,   0};

  s_state[port] = (((s_state[port] << 2) | (((PIND >> (1 + port)) & 0x02) | ((PIND >> (4 + port)) & 0x01))) & 0x0F);
  s_enc[port] += states[s_state[port]];
}

ISR(PCINT2_vect)
{
  static uint8_t PCintLast;

  // mask is pins that have changed. 
  uint8_t mask = PIND ^ PCintLast;

  // Update PCintLast for next time
  PCintLast = PIND;

  // screen out non pcint pins.
  mask &= 0x3C;

  if (mask & 0x14) { // PCINT 18 or 20, MAT
    Motor::update(0);
  }
  if (mask & 0x28) { // PCINT 19 or 21, MBT
    Motor::update(1);
  }
}

