/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#ifndef __Motor_h_
#define __Motor_h_

#include "Arduino.h"

class Motor
{
public:
  Motor(uint8_t port);
   
  enum Direction {
    Float,
    Locked,
    CW,
    CCW
  };
  
  static void setup();
  static void stop();
  void power(uint8_t dir, uint8_t pow);
  int32_t encoder() const;
  static void update(uint8_t port);

private:
  uint8_t m_port;
  int8_t m_lastDir;
  static uint8_t s_pins[][3];
  static volatile int32_t s_enc[2];
  static uint8_t s_state[2];
};

#endif
