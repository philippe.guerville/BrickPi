/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#include "Arduino.h"
#include "Sensor.h"
#include "BrickPi.h"

#define TYPE_MAX                101
#define MSG_TYPE_MASK           0xC0
#define MSG_CMD_MASK            0x7
#define MSG_MODE_MASK           0x7
#define MSG_SYS_MASK            0x7
#define CMD_SIZE(byte)          (1 << ((byte >> 3) & 0x7))
#define CMD_SELECT              0x43

enum type {
  SYS  = 0x00,
  CMD  = 0x40,
  INFO = 0x80,
  DATA = 0xC0,
};

enum sys {
  SYNC = 0x0,
  NACK = 0x2,
  ACK  = 0x4,
  ESC  = 0x6,
};

enum cmd {
  TYPE   = 0x0,
  MODES  = 0x1,
  SPEED  = 0x2,
  SELECT = 0x3,
  WRITE  = 0x4,
};

enum info {
  NAME   = 0x00,
  RAW    = 0x01,
  PCT    = 0x02,
  SI     = 0x03,
  UNITS	 = 0x04,
  FORMAT = 0x80,
};

enum data_type {
  DATA_8     = 0x00,
  DATA_16    = 0x01,
  DATA_32    = 0x02,
  DATA_FLOAT = 0x03
};

// sensor0 use pins 14 and 16
// sensor1 use pins 15 and 17
uint8_t Sensor::s_pins[][2] = {{14, 16}, {15, 17}};

Sensor::Sensor(uint8_t port):
  m_value(0),
  m_lastKeepalive(0),
  m_timeout(0),
  m_id(0),
  m_mode(0),
  m_dataHeader(0),
  m_numModes(0),
  m_speed(0),
  m_setupDone(false),
  m_port(port),
  m_serial(s_pins[port][0], s_pins[port][1])
{
  pinMode(s_pins[port][0], INPUT);
  pinMode(s_pins[port][1], OUTPUT);
}

void Sensor::setup(byte mode)
{
  // setup of the ultrasonic sensor can take almost 4s
  // worst case: 1 uart sensor on MCU1 and 2 uart sensors on MCU2
  // MCU1 will have no update messages before the complete setup of those 3 sensors
  // set shutdown timeout temporarly to 3*4s -> 12000 ms
  // this timeout will be reset to default value at the next brickpi update
  brickpi.setTimeout(millis() + 12000);
#if(0)
  unsigned long t0 = millis();
#endif
  reset();
  
  // this setup is relevant only for uart sensor not for touch sensor
  if (!isUart()) {
    m_id = Touch;
    m_setupDone = true;
    return;
  }

  byte cmd, type, chksum, frame[64];
  
  boolean synced = false;
  m_serial.begin(2400);

  for (int i = 0; i< 10 ; i++)
    m_serial.write(SYNC);

  // ultrasonic sensor can take almost 4s to completely send all his informations
  m_timeout = millis() + 4000;

  if (!read(&cmd)) return;

  // we don't know where we are in the setup sequence of the sensor
  // read until we detect the correct 3 bytes sequence (CMD|TYPE, type, chksum)
  while (!synced) {
    if (!read(&type)) return;
    if (!peek(&chksum)) return;

    if (cmd != (CMD | TYPE) || 
      !type || type > TYPE_MAX ||
      chksum != (0xFF ^ cmd ^ type)) {
      cmd = type;
      continue;
    }
    if (!read(&chksum)) return;
    synced = true;
    m_id = type;
#if(0)
    brickpi.debug("Device id %d", m_id);
#endif
  }
#if(0)
  brickpi.debug("Synced!");
#endif

  // once synchronized, we can read all the the frames from the the sensor
  while (!m_setupDone) {
    int count;
    if (!readFrame(frame, &count)) return;
    if (XOR(frame, count)) {
      brickpi.debug("Checksum KO!");
    }
    processFrame(frame, count);
  }
  
  // only one SoftwareSerial can listen
  // stop listening for now
  m_serial.stopListening();
    
  // determine the first byte that we must detect in the data frame
  setDataHeader(mode);
}

void Sensor::reset()
{  
  if (m_setupDone) brickpi.debug("Reset !!!!!!");
  m_value = 0;
  m_lastKeepalive = 0;
  m_timeout = 0;
  m_id = 0;
  m_mode = 0;
  m_dataHeader = 0;
  m_numModes = 0;
  m_speed = 0;
  m_setupDone = false;
  m_serial.end();
}

void Sensor::keepalive()
{
  if (m_setupDone && (m_id != Touch) &&
      ((millis() - m_lastKeepalive) > 80)) 
  {
     m_serial.write(NACK);
     m_lastKeepalive = millis();   
  }
}

void Sensor::update(byte mode)
{
  if (!m_setupDone) {
    setup(mode);
    m_lastKeepalive = millis();
  }

  if (m_setupDone) {
    if (m_id != Touch) {
      updateUartValue(mode);
    } else {
      updateAnalogicValue();
    }
  }
}

byte Sensor::id() const
{
  return m_id;
}

byte Sensor::mode() const
{
  return m_mode;
}

int16_t Sensor::value() const
{
  return m_value;
}

boolean Sensor::read(byte* val)
{
  // define a read with timeout function
  while (!m_serial.available()) {
    if (millis() > m_timeout) 
      return false;
  };

  *val = m_serial.read();

  return true;
}

boolean Sensor::peek(byte* val)
{
  //define a peek with timeout function
  while (!m_serial.available()) {
    if (millis() > m_timeout) 
      return false;
  };

  *val = m_serial.peek();
  return true;
}

int Sensor::msgSize(byte header)
{
  if (!(header & MSG_TYPE_MASK)) /* SYNC, NACK, ACK */
    return 1;

  int size = CMD_SIZE(header);
  size += 2; /* header and checksum */
  if ((header & MSG_TYPE_MASK) == INFO)
    size++; /* extra command byte */

  return size;
}

boolean Sensor::readFrame(byte* frame, int* count)
{
  if (!read(frame)) return false;
      
  *count = msgSize(*frame);
  for (int i = 1; i < *count; i++) {
    if (!read(frame+ i)) return false;
  }
  
  return true;
}

boolean Sensor::processFrame(byte* frame, int count)
{
  byte type = frame[0] & MSG_TYPE_MASK;
  
  // we must read all the frame, even if we do not use the information
  // to stay synchronised with the sensor
  switch(type) {
  case SYS:
    processSys(frame, count);
    break;
  case CMD:
    processCmd(frame, count);
    break;
  case INFO:
    processInfo(frame, count);
    break;
  case DATA:
    processData(frame, count);
    break;
  default:
    brickpi.debug("Type unknown %d", type);
  }
  
  return true;
}

boolean Sensor::processCmd(byte* frame, int count)
{
  byte cmd =  frame[0] & MSG_CMD_MASK;
  
  switch(cmd) {
  case TYPE:
    brickpi.debug("CMD TYPE");
    break;
  case MODES:
    if (m_numModes) brickpi.debug("Received duplicated mode info");
    m_numModes = frame[1] + 1;
#if(0)
    brickpi.debug("CMD MODES %d", m_numModes);
#endif
    break;
  case SPEED:
    if (m_speed) brickpi.debug("Received duplicated speed info");
    m_speed = *(unsigned long*)(frame + 1);
#if(0)
    brickpi.debug("CMD SPEED %u", m_speed);
#endif
    break;
  default:
    brickpi.debug("Cmd unknown %d", cmd);
  }  
  return true;
}

boolean Sensor::processInfo(byte* frame, int count)
{
  byte mode =  frame[0] & MSG_MODE_MASK;
  byte info = frame[1];
  
  switch(info) {
  case NAME:
#if(0)
    frame[count - 1] = '\0';
    brickpi.debug("INFO NAME %d %s", mode, frame + 2);
#endif
    break;
  case RAW:
    if (count < 11) brickpi.debug("INFO RAW message too short %d", count);
#if(0)
    else {
      unsigned long low = *(unsigned long*)(frame + 2);
      unsigned long high = *(unsigned long*)(frame + 6);
      brickpi.debug("INFO RAW %u %u", low, high);
    }
#endif
    break;
  case PCT:
    if (count < 11) brickpi.debug("INFO PCT message too short %d", count);
#if(0)
    else {
      unsigned long low = *(unsigned long*)(frame + 2);
      unsigned long high = *(unsigned long*)(frame + 6);
      brickpi.debug("INFO PCT %u %u", low, high);
    }
#endif
    break;
  case SI:
    if (count < 11) brickpi.debug("INFO SI message too short %d", count);
#if(0)
    else {
      unsigned long low = *(unsigned long*)(frame + 2);
      unsigned long high = *(unsigned long*)(frame + 6);
      brickpi.debug("INFO SI %u %u", low, high);
    }
#endif
    break;
  case UNITS:
#if(0)
    frame[count - 1] = '\0';
    brickpi.debug("INFO UNITS %d %s", mode, frame + 2);
#endif
    break;
  case FORMAT:
    if (count < 7) brickpi.debug("INFO FORMAT message too short %d", count);
    else {
#if(0)
      brickpi.debug("INFO FORMAT %u %u %u %u %u", mode, frame[2], frame[3], frame[4], frame[5]);
#endif
      m_formats[mode][0] = frame[2]; // Number of data sets
      m_formats[mode][1] = frame[3]; // Data set format
    }
    break;
  default:
    brickpi.debug("Info unknown %d", info);
  }  
  return true;
}

boolean Sensor::processSys(byte* frame, int count)
{
  byte sys = frame[0] & MSG_SYS_MASK;
  
  switch(sys) {
  case SYNC:
#if(0)
    brickpi.debug("SYS SYNC");
#endif
    break;
  case ACK:
    // this is the last message of the setup
    // only now we can send an acknowledgement and switch to the new communication speed
#if(0)
    brickpi.debug("SYS ACK");
#endif
    m_serial.write(ACK);
    m_serial.end();
    m_serial.begin(m_speed);
    m_setupDone = true;
    break;
  default:
    brickpi.debug("Sys unknown %d", sys);
  }  
  return true;
}

boolean Sensor::processData(byte* frame, int count)
{
  m_mode =  frame[0] & MSG_MODE_MASK;
  
  m_value = 0;
  
  switch (m_formats[m_mode][1]) {
    case DATA_8:
      m_value = *((int8_t*)(frame+1));
      break;
    case DATA_16:
      m_value = *((int16_t*)(frame+1));
      break;
  }

  return true;
}

void Sensor::setDataHeader(byte mode)
{
  // determine the first byte that we must detect in the data frame 
  // based on the current mode and on the sensor information
  byte dataSetSize = (m_formats[mode][1] != DATA_FLOAT) ? m_formats[mode][1] : DATA_32;
  dataSetSize = 1 << dataSetSize;
  dataSetSize *= m_formats[mode][0];

  byte payload = 0;
  while (dataSetSize >>= 1) payload++;

  m_dataHeader = DATA | (payload << 3) | mode;
#if(0) 
  brickpi.debug("setDataHeader %d %s", m_dataHeader, byte_to_binary(m_dataHeader));
#endif
}

void Sensor::setMode(byte mode)
{
  // send (if needed) the select command to swith the sensor mode
  mode = (mode >= m_numModes) ? (m_numModes - 1) : mode;
  if (m_mode != mode) {
    m_serial.write(CMD_SELECT);
    m_serial.write(mode);
    m_serial.write(0xFF^CMD_SELECT^mode);
    // recompute the data header based on the new mode
    setDataHeader(mode);
  }
}

void Sensor::updateUartValue(byte mode)
{
  setMode(mode);
  
  byte header, frame[32];  
  boolean synced = false;
  
  m_timeout = millis() + 20;

  m_serial.listen();
  // ask the sensor to send again his value
  m_serial.write(NACK);
  m_lastKeepalive = millis();

  // we don't know were we are in the data sequence
  // we first try to detect the correct data header
  while (!synced) {
    if (!peek(&header)) {
      m_serial.stopListening();
#if(0)
      brickpi.debug("Peek data header timout");
#endif
      return;
    }
    if (header == m_dataHeader) {
      synced = true;
    } else {
      if (!read(&header)) {
        m_serial.stopListening();
#if(0)
        brickpi.debug("Read data header timout");
#endif
        return;
      }
    }
  }

  int count;
  if (!readFrame(frame, &count)) {
      m_serial.stopListening();
#if(0)
      brickpi.debug("Read data frame timout, count %d", count);
#endif
      return;
  }

  m_serial.stopListening();
  
  if (XOR(frame, count)) {
#if(0)
    brickpi.debug("Data frame checksum KO!");
    for (int i = 0; i < count; ++i) {
      brickpi.debug("%d", frame[i]);
    }
#endif
    return;
  }

  // the sensor value is updated only if we have correcly read
  // the header, the data and verified the checksum
  processData(frame, count);
}

void Sensor::updateAnalogicValue() 
{
  // for the touch sensor, we just make an analogic read
  m_value = analogRead(s_pins[m_port][0]);
}

boolean Sensor::isUart()
{
  // this function 'guess' if we are connected to a uart sensor or not
  // analogRead on a uart sensor return only 0 or 1023
  // analogRead on a touch sensor return random noise or 1023, never 0
  // if someone has a better idea ......
  int count = 0;
  
  for (int i = 0; i < 100; i++) {
    if (analogRead(s_pins[m_port][0]) == 0) count++;
  }
#if(0)  
  brickpi.debug("%s %d port %d count %d", __PRETTY_FUNCTION__, __LINE__, m_port, count);
#endif
  return (count > 30);
}

