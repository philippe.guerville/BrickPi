/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#ifndef SENSOR_H
#define SENSOR_H

#include "SoftwareSerial.h"

class Sensor
{
public:
  Sensor(uint8_t port);
     
  void setup(byte mode);
  void reset();
  void keepalive();
  void update(byte mode);
  byte id() const;
  byte mode()const;
  int16_t value() const;

  enum Type {
    Touch = 0,
    Color = 29,
    Ultrasonic = 30,
    Gyro = 32,
    Infrared = 33
  };

private:
  boolean read(byte* val);
  boolean peek(byte* val);
  int msgSize(byte header);
  boolean readFrame(byte* frame, int* count);
  boolean processFrame(byte* frame, int count);
  boolean processCmd(byte* frame, int count);
  boolean processInfo(byte* frame, int count);
  boolean processSys(byte* frame, int count);
  boolean processData(byte* frame, int count);
  void setDataHeader(byte mode);
  void setMode(byte mode);
  void updateUartValue(byte mode);
  void updateAnalogicValue();
  boolean isUart();

  int16_t m_value;
  volatile unsigned long m_lastKeepalive;
  unsigned long m_timeout;
  byte m_id;
  byte m_mode;
  byte m_dataHeader;
  byte m_numModes;
  byte m_formats[8][2];
  unsigned long m_speed;
  volatile boolean m_setupDone;
  uint8_t m_port;
  SoftwareSerial m_serial;
  static uint8_t s_pins[][2];
};

#endif
