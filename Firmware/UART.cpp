/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#include "UART.h"
#include "Message.h"
#include <EEPROM.h>

UART::UART()
{
  m_address = 0;
}

void UART::setup()
{
  Serial.begin(500000);
  // Serial read is non blocking with a default 1s timeout
  // chanage it to 50 ms
  Serial.setTimeout(50);
  m_address = EEPROM.read(0);
}

boolean UART::send(Message& msg)
{
  // this message is send by this MCU (MCU1 or MCU2)
  msg.recipient= m_address;
  // compute and store the checksum
  msg.finalize();
  return write(&msg, sizeof(Header) + msg.length);
}

boolean UART::receive(Message* msg)
{
  msg->reset();
  
  /**
   * we must read the message completly, even if it is not for this mcu
   * in order to remain synchronised with the raspberry pi
   *
   * if we have been able to read the header and the paylod before Serial timeout
   * if the checksum of this message is ok
   * if this message is for this MCU
   */
  if (read(msg, sizeof(Header)) && read(msg->data, msg->length) && msg->validate()) {
    return msg->recipient == m_address;
  } else {
    // discard the remaining input buffer only if something bad append (timeout or wrong checksum)
    flush();
  }
  return false;
}

boolean UART::read(void* buf, size_t count)
{
  return Serial.readBytes((char*)buf, count) == count;
}

boolean UART::write(const void* buf, size_t count)
{
  return Serial.write((const byte*)buf, count) == count;
}

void UART::flush()
{
  while (Serial.available()) {
    Serial.read();
  } 
}

