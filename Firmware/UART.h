/**
 *  Philippe Guerville
 *  https://github.com/guervillep/BrickPi
 *
 *  Credits
 *  Matthew Richardson *  matthewrichardson37<at>gmail.com *  http://mattallen37.wordpress.com/
 *  Jaikrishna T S *  t.s.jaikrishna<at>gmail.com
 *  John Cole at Dexter Industries.
 *  David Lechner <david@lechnology.com> * ev3dev
 *
 *  You may use this code as you wish, provided you give credit where it's due.
 */

#ifndef UART_H
#define UART_H

#include "Arduino.h"

class Message;

class UART
{
public:
  UART();

  void setup();
  boolean send(Message& msg);
  boolean receive(Message* msg);

private:
  boolean read(void* buf, size_t count);
  boolean write(const void* buf, size_t count);
  void flush();

  uint8_t m_address;  
};

#endif


