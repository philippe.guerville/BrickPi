SUBDIRS = driver test explorer

CC = arm-linux-gnueabihf-g++
AR = arm-linux-gnueabihf-ar

all:
	for dir in $(SUBDIRS); do \
	  $(MAKE) -C $$dir CC=$(CC) AR=$(AR); \
	done

clean:
	for dir in $(SUBDIRS); do \
	  $(MAKE) -C $$dir clean; \
	done
