BrickPi EV3
=====

This repository holds an alternate firmware and driver for the BrickPi

[Dexter Industries] (http://www.dexterindustries.com/)
[http://www.dexterindustries.com/BrickPi] (http://www.dexterindustries.com/BrickPi)

These files have been made available online through a [Creative Commons Attribution-ShareAlike 3.0](http://creativecommons.org/licenses/by-sa/3.0/) license.

They are based on the original Birckpi firmware.

[BrickPi Firmware 2.5] (https://github.com/DexterInd/BrickPi/tree/master/Firmware_BrickPi/Firmware_2.5)

And on the ev3dev Linux drivers.

[ev3dev uart sensor linux driver] (https://github.com/ev3dev/lego-linux-drivers/blob/master/sensors/ev3_uart_sensor_ld.c)
