#include "BrickPi.h"
#include "UART.h"
#include "Motor.h"
#include "Sensor.h"
#include "Message.h"

namespace BPi {

BrickPi* BrickPi::s_instance = NULL;

BrickPi::BrickPi()
{
    m_uart = new UART();
    for (int port = 0 ; port < 4; port++) {
        m_motors[port] = NULL;
        m_sensors[port] = NULL;
    }
}

BrickPi::~BrickPi()
{
    delete m_uart;
    for (int port = 0 ; port < 4; port++) {
        delete m_motors[port];
        delete m_sensors[port];
    }
}

BrickPi* BrickPi::instance()
{
    if (!s_instance) {
        s_instance = new BrickPi();
        if (!s_instance->m_uart->open())
            close();
    }

    return s_instance;
}

void BrickPi::close()
{
    delete s_instance;
    s_instance = NULL;
}

Motor* BrickPi::motor(Port port)
{
    if (!m_motors[port])
        m_motors[port] = new Motor();

    return m_motors[port];
}

Sensor* BrickPi::sensor(Port port)
{
    if (!m_sensors[port]) {
        m_sensors[port] = new Sensor();
        m_uart->setTimeout(4000000);
        updateSensor(port);
        m_uart->setTimeout(25000);
    }
    return m_sensors[port];
}

bool BrickPi::update()
{
    bool success = true;

    for (int port = 0 ; port < 4; port++) {
        if (m_motors[port])
            success &= updateMotor(port);
        if (m_sensors[port])
            success &= updateSensor(port);
    }

    return success;
}

bool BrickPi::updateMotor(int port)
{
    uint8_t mcu = (port / 2) ? UART::MCU2 : UART::MCU1;
    uint8_t type = (port % 2) ? Message::MOTOR1 : Message::MOTOR0;

    Message answer, request(mcu, type, 2);
    request.data[0] = m_motors[port]->direction();
    request.data[1] = m_motors[port]->power();

    if (!m_uart->send(request))
        return false;

    if (!m_uart->receive(&answer))
        return false;

    int32_t* ptr = (int32_t*)answer.data;
    m_motors[port]->setEncoder(*ptr);

    return true;
}

bool BrickPi::updateSensor(int port)
{
    uint8_t mcu = (port / 2) ? UART::MCU2 : UART::MCU1;
    uint8_t type = (port % 2) ? Message::SENSOR1 : Message::SENSOR0;

    Message answer, request(mcu, type, 1);
    request.data[0] = m_sensors[port]->requestedMode();

    if (!m_uart->send(request))
        return false;

    if (!m_uart->receive(&answer))
        return false;

    m_sensors[port]->setId(answer.data[0]);
    m_sensors[port]->setCurrentMode(answer.data[1]);
    int16_t* ptr = (int16_t*)(answer.data + 2);
    m_sensors[port]->setValue(*ptr);
    return true;
}

}
