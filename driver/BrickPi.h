#ifndef BRICKPI_H
#define BRICKPI_H

namespace BPi {

enum Port
{
    MA = 0,
    MB = 1,
    MC = 2,
    MD = 3,
    S1 = 0,
    S2 = 1,
    S3 = 2,
    S4 = 3
};

class UART;
class Motor;
class Sensor;

class BrickPi
{
public:
    static BrickPi* instance();
    static void close();
    Motor* motor(Port port);
    Sensor* sensor(Port port);
    bool update();

private:
    BrickPi();
    ~BrickPi();
    BrickPi(const BrickPi&);
    void operator = (const BrickPi&);

    bool updateMotor(int port);
    bool updateSensor(int port);

    UART* m_uart;
    Motor* m_motors[4];
    Sensor* m_sensors[4];
    static BrickPi* s_instance;
};

}

#endif // BRICKPI_H
