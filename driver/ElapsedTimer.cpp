#include "ElapsedTimer.h"
#include "time.h"
#include <iostream>
#include <errno.h>
#include <string.h>

using namespace std;

namespace BPi {

ElapsedTimer::ElapsedTimer():
    mt0(0),
    ut0(0),
    nt0(0)
{
    start();
}

void ElapsedTimer::now() const
{
    struct timespec now;

    if (clock_gettime(CLOCK_MONOTONIC, &now)) {
        cerr << strerror(errno) << endl;
        return;
    }

    mt1 = now.tv_sec * 1000LL + now.tv_nsec / 1000000LL;
    ut1 = now.tv_sec * 1000000LL + now.tv_nsec / 1000LL;
    nt1 = now.tv_sec * 1000000000LL + now.tv_nsec;
}

void ElapsedTimer::start()
{
    mRestart();
}

void ElapsedTimer::reset()
{
    mt0 = mt1;
    ut0 = ut1;
    nt0 = nt1;
}

int64_t ElapsedTimer::mRestart()
{
    int64_t elapsed = mElapsed();
    reset();
    return elapsed;
}

int64_t ElapsedTimer::uRestart()
{
    int64_t elapsed = uElapsed();
    reset();
    return elapsed;
}

int64_t ElapsedTimer::nRestart()
{
    int64_t elapsed = nElapsed();
    reset();
    return elapsed;
}

int64_t ElapsedTimer::mElapsed() const
{
    now();
    return (mt1 - mt0);
}

int64_t ElapsedTimer::uElapsed() const
{
    now();
    return (ut1 - ut0);
}

int64_t ElapsedTimer::nElapsed() const
{
    now();
    return (nt1 - nt0);
}


}
