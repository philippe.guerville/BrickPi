#ifndef ELAPSEDTIMER_H
#define ELAPSEDTIMER_H

#include <stdint.h>

namespace BPi {

class ElapsedTimer
{
public:
    ElapsedTimer();
    void start();
    void reset();
    int64_t mRestart();
    int64_t uRestart();
    int64_t nRestart();
    int64_t mElapsed() const;
    int64_t uElapsed() const;
    int64_t nElapsed() const;

private:
    void now() const;

    int64_t mt0;
    int64_t ut0;
    int64_t nt0;
    mutable int64_t mt1;
    mutable int64_t ut1;
    mutable int64_t nt1;
};

}

#endif // ELAPSEDTIMER_H
