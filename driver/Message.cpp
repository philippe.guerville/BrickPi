#include "Message.h"
#include <string.h>
#include <iostream>

using namespace std;

namespace BPi {

Header::Header(uint8_t recipient, uint8_t type, uint8_t length):
    recipient(recipient),
    type(type),
    length(length),
    chksum(0)
{
}

void Header::reset()
{
    memset(this, 0, sizeof(Header));
}

Message::Message(uint8_t recipient, uint8_t type, uint8_t length):
    Header(recipient, type, length)
{
}

void Message::finalize()
{
    chksum = 0;
    chksum = XOR(this, sizeof(Header) + length);
}

bool Message::validate() const
{
    return XOR(this, sizeof(Header) + length) == 0x00;
}

uint8_t XOR(const void* data, size_t size)
{
    uint8_t chksum = 0xFF;
    const uint8_t* ptr = (const uint8_t*)data;
    const uint8_t* end = ptr + size;

    while (ptr < end) chksum ^= *ptr++;

    return chksum;
}

}
