#ifndef MESSAGE_H
#define MESSAGE_H

#include <stdlib.h>
#include <stdint.h>

namespace BPi {

struct Header
{
    Header(uint8_t recipient, uint8_t type, uint8_t length);

    void reset();

    uint8_t recipient;
    uint8_t type;
    uint8_t length;
    uint8_t chksum;
};

struct Message : public Header
{
    Message(uint8_t recipient = 0, uint8_t type = VOID, uint8_t length = 0);

    enum Type {
        VOID,
        DBG,
        MOTOR0,
        MOTOR1,
        SENSOR0,
        SENSOR1
    };

    void finalize();
    bool validate() const;

    char data[256];
};

uint8_t XOR(const void* data, size_t size);

}

#endif // MESSAGE_H
