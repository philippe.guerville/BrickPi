#include "Motor.h"

namespace BPi {

Motor::Motor():
    m_encoder(0),
    m_direction(Float),
    m_power(0)
{
}

Motor::~Motor()
{
}

uint8_t Motor::direction() const
{
    return m_direction;
}

uint8_t Motor::power() const
{
    return m_power;
}

void Motor::power(uint8_t dir, uint8_t pow)
{
    m_direction = dir;
    m_power = pow;
}

int32_t Motor::encoder() const
{
    return m_encoder;
}

void Motor::setEncoder(int32_t encoder)
{
    m_encoder = encoder;
}


}
