#ifndef MOTOR_H
#define MOTOR_H

#include <stdint.h>

namespace BPi {

class BrickPi;

class Motor
{
public:
    enum Direction {
        Float,
        Locked,
        CW,
        CCW
    };

    uint8_t direction() const;
    uint8_t power() const;
    void power(uint8_t dir, uint8_t pow = 0);
    int32_t encoder() const;

private:
    Motor();
    ~Motor();
    Motor(const Motor&);
    void operator = (const Motor&);

    void setEncoder(int32_t encoder);

    int32_t m_encoder;
    uint8_t m_direction;
    uint8_t m_power;

    friend class BrickPi;
};

}

#endif // MOTOR_H
