#include "Sensor.h"

namespace BPi {

Sensor::Sensor():
    m_id(0),
    m_requestedMode(0),
    m_currentMode(0),
    m_value(0)
{
}

Sensor::~Sensor()
{
}

uint8_t Sensor::id()
{
    return m_id;
}

uint8_t Sensor::mode() const
{
    return m_currentMode;
}

void Sensor::setMode(uint8_t mode)
{
    m_requestedMode = mode;
}

int16_t Sensor::value() const
{
    return m_value;
}

uint8_t Sensor::requestedMode() const
{
    return m_requestedMode;
}

void Sensor::setId(int8_t id)
{
    m_id = id;
}

void Sensor::setCurrentMode(int8_t mode)
{
    m_currentMode = mode;
}

void Sensor::setValue(int16_t value)
{
    m_value = value;
}

}
