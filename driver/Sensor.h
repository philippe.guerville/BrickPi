#ifndef SENSOR_H
#define SENSOR_H

#include <stdint.h>

namespace BPi {

class Sensor
{
public:
    uint8_t id();
    uint8_t mode() const;
    void setMode(uint8_t mode);
    int16_t value() const;

private:
    Sensor();
    ~Sensor();
    Sensor(const Sensor&);
    void operator = (const Sensor&);

    uint8_t requestedMode() const;
    void setId(int8_t id);
    void setCurrentMode(int8_t mode);
    void setValue(int16_t value);

    uint8_t m_id;
    uint8_t m_requestedMode;
    uint8_t m_currentMode;
    int16_t m_value;

    friend class BrickPi;
};

}

#endif // SENSOR_H
