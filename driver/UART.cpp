#include "UART.h"
#include "Message.h"
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <iostream>

using namespace std;

namespace BPi {

UART::UART():
    m_fd(0),
    m_usec(25000)
{
}

UART::~UART()
{
    close(m_fd);
}

bool UART::open()
{
    struct termios options;

    m_fd = ::open("/dev/ttyAMA0", O_RDWR|O_NOCTTY|O_NDELAY|O_SYNC);
    if (m_fd == -1) {
        cerr << "open " << strerror(errno) << endl;
        return false;
    }

    if (tcgetattr(m_fd, &options) < 0) {
        cerr << "tcgetattr " << strerror(errno) << endl;
        return -1;
    }

    options.c_cflag = B500000 | CS8 | CLOCAL | CREAD;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;

    if (tcflush(m_fd, TCIFLUSH) < 0) {
        cerr << "tcflush " << strerror(errno) <<endl;
        return false;
    }

    if (tcsetattr(m_fd, TCSANOW, &options) < 0) {
        cerr << "tcsetattr " << strerror(errno) << endl;
        return false;
    }

    return true;
}

bool UART::send(Message &msg)
{
    msg.finalize();
    return write(&msg, sizeof(Header) + msg.length);
}

bool UART::receive(Message* msg)
{
    msg->reset();

    do {
        if (read(msg, sizeof(Header)) && read(msg->data, msg->length) && msg->validate()) {
            if (msg->type == Message::DBG) {
                msg->data[msg->length] = '\0';
                cerr << "DBG from " << (int)msg->recipient << ": " << msg->data << endl;
            } else {
                return true;
            }
        } else {
            break;
        }
    } while (msg->type == Message::DBG);

    flush();
    return false;
}

void UART::setTimeout(long usec)
{
    m_usec = usec;
}

bool UART::read(void* buf, size_t count)
{
    // null length data
    if (!count) return true;

    fd_set readfds;
    struct timeval timeout;
    ssize_t bytes;
    int n;
    char* ptr = (char*)buf;

    // wait for bytes available
    do {
        FD_ZERO(&readfds);
        FD_SET(m_fd, &readfds);

        timeout.tv_sec  = m_usec / 1000000;
        timeout.tv_usec = m_usec % 1000000;

        n = select(m_fd + 1, &readfds, NULL, NULL, &timeout);

        if (n < 0) {
            cerr << "select " << strerror(errno) << endl;
            return false;
        }

        if (n == 0) {
            cerr << "select timeout " << endl;
            return false;
        }

        bytes = ::read(m_fd, ptr, count);

        if (bytes < 0) {
            cerr << "read " << strerror(errno) << endl;
            return false;
        }

        if (bytes == 0) {
            cerr << "read broken uart connection" << endl;
            return false;
        }

        count -= bytes;
        ptr += bytes;
    } while(count);

    return true;
}

bool UART::write(const void* buf, size_t count)
{
    ssize_t bytes = ::write(m_fd, buf, count);

    if (bytes != (ssize_t)count)
        cerr << "write " << strerror(errno) << endl;

    return (bytes == (ssize_t)count);
}

void UART::flush()
{
    // flushes data received but not read
    tcflush(m_fd, TCIFLUSH);
}

}
