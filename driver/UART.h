#ifndef UART_H
#define UART_H

#include <stdint.h>
#include <stdlib.h>

namespace BPi {

struct Message;
class BrickPi;

class UART
{
public:
    // Micro Controller Unit addresses
    enum Controller {
        MCU1 = 1,
        MCU2 = 2
    };

    bool open();
    bool send(Message &msg);
    bool receive(Message* msg);
    void setTimeout(long usec);

private:
    UART();
    ~UART();
    UART(const UART&);
    void operator = (const UART&);

    bool read(void* buf, size_t count);
    bool write(const void *buf, size_t count);
    void flush();

    int m_fd;
    long m_usec;

    friend class BrickPi;
};

}

#endif // UART_H
