TEMPLATE = lib
TARGET = brickpi
CONFIG += console staticlib
CONFIG -= qt

SOURCES += \
    Message.cpp \
    UART.cpp \
    Motor.cpp \
    BrickPi.cpp \
    Sensor.cpp \
    ElapsedTimer.cpp

HEADERS += \
    Message.h \
    UART.h \
    Motor.h \
    BrickPi.h \
    Sensor.h \
    ElapsedTimer.h
