#include "LineFollower.h"
#include "PID.h"
#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

LineFollower::LineFollower(Motor *lWheel, Motor *rWheel, Sensor *color):
    m_lWheel(lWheel),
    m_rWheel(rWheel),
    m_color(color),
    m_target(0.0),
    m_power(0)
{
    m_pid = new PID(0.0550, 0.0055, 0.0012);
}

LineFollower::~LineFollower()
{
    delete m_pid;
}

void LineFollower::start(double target, uint8_t power)
{
    m_target = target;
    m_power = power;
    m_lWheel->power(Motor::CW, m_power);
    m_rWheel->power(Motor::CW, m_power);
    BrickPi::instance()->update();
    m_pid->init(m_target - m_color->value());
}

void LineFollower::update()
{
    BrickPi::instance()->update();
    if (m_color->value() > 100) {
        cerr << "color bad value" << endl;
        exit(EXIT_FAILURE);
    }

    double p = m_pid->update(m_target - m_color->value());
    cout << setw(10) << m_lWheel->encoder();
    cout << setw(10) << m_color->value();
    cout << endl;
    turn(p);
}

void LineFollower::turn(double value)
{
    if (value >= 0) {
        m_lWheel->power(Motor::CW, m_power);
        value = (value > 2.0) ? 2.0 : value;
        if (value < 1.0)
            m_rWheel->power(Motor::CW, (uint8_t)(m_power * (1.0 - value)));
        else
            m_rWheel->power(Motor::CCW, (uint8_t)(m_power * (value - 1.0)));
    } else {
        value = (value < -2.0) ? -2.0 : value;
        if (value > -1.0)
            m_lWheel->power(Motor::CW, (uint8_t)(m_power * (1.0 + value)));
        else
            m_lWheel->power(Motor::CCW, (uint8_t)(m_power * (-value - 1.0)));
        m_rWheel->power(Motor::CW, m_power);
    }
}
