#ifndef LINE_FOLLOWER_H
#define LINE_FOLLEWER_H

#include "BrickPi.h"
#include "Motor.h"
#include "Sensor.h"

using namespace BPi;
class PID;

class LineFollower
{
public:
    LineFollower(Motor* lWheel, Motor* rWheel, Sensor* color);
    ~LineFollower();

    void start(double target, uint8_t power);
    void update();

private:
    void turn(double value);

    Motor* m_lWheel;
    Motor* m_rWheel;
    Sensor* m_color;
    PID* m_pid;
    double m_target;
    uint8_t m_power;
};

#endif
