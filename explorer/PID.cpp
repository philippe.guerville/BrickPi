#include "PID.h"
#include <iostream>
#include <iomanip>

using namespace std;

PID::PID(double kp, double ki, double kd):
    m_kp(kp),
    m_ki(ki),
    m_kd(kd),
    m_integral(0),
    m_numSamples(7),
    m_it(0),
    m_slope(0),
    m_intercept(0)
{
    m_error = new double[m_numSamples];
    m_time = new double[m_numSamples];
}

PID::~PID()
{
    delete[] m_error;
    delete[] m_time;
}

void PID::init(double error)
{
    m_it = 0;
    m_lastError = error;
    m_integral = 0;
    m_error[0] = error;
    m_time[0] = 0;
    m_it++;
    m_timer.start();
}

double PID::update(double error)
{
    m_error[m_it % m_numSamples] = error;
    m_time[m_it % m_numSamples] = m_timer.nElapsed() / 1000000000.0;
    linearFit();

    double dt = m_time[m_it % m_numSamples] - m_time[(m_it - 1) % m_numSamples];
    error = m_intercept;

    m_integral += error * dt;
    double derivative = (error - m_lastError) / dt;
    double result = m_kp * error + m_ki * m_integral + m_kd * derivative;

    cout << setw(12) << dt;
    cout << setw(12) << m_kp * error;
    cout << setw(12) << m_ki * m_integral;
    cout << setw(12) << m_kd * derivative;
    cout << setw(12) << result;

    m_lastError = error;

    m_it++;

    return result;
}

void PID::linearFit()
{
    double sumx = 0;
    double sumx2 = 0;
    double sumxy = 0;
    double sumy = 0;
    double sumy2 = 0;

    for (int i = 0; i < m_numSamples; i++) {
        double x = m_time[i] - m_time[m_it % m_numSamples];
        double y = m_error[i];
        sumx  += x;
        sumx2 += x * x;
        sumxy += x * y;
        sumy  += y;
        sumy2 += y * y;
    }

    double denom = (m_numSamples * sumx2 - sumx * sumx);

    if (denom == 0) {
        cerr << "singular matrix. can't solve the problem" << endl;
        return;
    }

    m_slope = (m_numSamples * sumxy  -  sumx * sumy) / denom;
    m_intercept = (sumy * sumx2  -  sumx * sumxy) / denom;
}
