#ifndef PID_H
#define PID_H

#include "ElapsedTimer.h"

using namespace BPi;

class PID
{
public:
    PID(double kp, double ki = 0, double kd = 0);
    ~PID();
    void init(double error);
    double update(double error);

private:
    void linearFit();

    double m_kp;
    double m_ki;
    double m_kd;
    double m_integral;
    double m_lastError;
    int m_numSamples;
    double* m_error;
    double* m_time;
    int m_it;
    double m_slope;
    double m_intercept;
    ElapsedTimer m_timer;
};

#endif
