TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += \
    main.cpp \
    LineFollower.cpp \
    PID.cpp

HEADERS += \
    LineFollower.h \
    PID.h

LIBS += -L$$OUT_PWD/../driver/ -lbrickpi

INCLUDEPATH += $$PWD/../driver
DEPENDPATH += $$PWD/../driver

PRE_TARGETDEPS += $$OUT_PWD/../driver/libbrickpi.a
