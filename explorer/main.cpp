#include "BrickPi.h"
#include "Motor.h"
#include "Sensor.h"
#include "LineFollower.h"

#include "ElapsedTimer.h"
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

using namespace std;
using namespace BPi;

int main()
{
    BrickPi* bpi = BrickPi::instance();

    if (!bpi) return EXIT_FAILURE;

    ElapsedTimer timer;

    LineFollower lineFollower(bpi->motor(MA),
                              bpi->motor(MD),
                              bpi->sensor(S3));

    cout << "setup took " << timer.mRestart() << "ms" << endl;

    lineFollower.start(20, 96);

    int count = 0;
    timer.start();

    while (bpi->motor(MA)->encoder() < 27000) {
        lineFollower.update();
        count++;
    }

    bpi->motor(MA)->power(Motor::Float);
    bpi->motor(MD)->power(Motor::Float);
    bpi->update();

    cout << count << " "
         << (double)timer.mElapsed()/count << "ms"
         << " " << (double)timer.mElapsed()/1000 << endl;

    BrickPi::close();

    return EXIT_SUCCESS;
}
