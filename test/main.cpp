#include "BrickPi.h"
#include "Motor.h"
#include "Sensor.h"
#include "ElapsedTimer.h"
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

using namespace std;
using namespace BPi;

int main()
{
    BrickPi* bpi = BrickPi::instance();

    if (!bpi) return EXIT_FAILURE;

    ElapsedTimer timer;
    Sensor* s1 = bpi->sensor(S1);
    cout << "s1 took " << timer.mRestart() << "ms" << endl;
    Sensor* s2 = bpi->sensor(S2);
    cout << "s2 took " << timer.mRestart() << "ms" << endl;
    Sensor* s3 = bpi->sensor(S3);
    cout << "s3 took " << timer.mRestart() << "ms" << endl;
    Sensor* s4 = bpi->sensor(S4);
    cout << "s4 took " << timer.mRestart() << "ms" << endl;
    Motor* ma = bpi->motor(MA);
    Motor* mb = bpi->motor(MB);
    Motor* mc = bpi->motor(MC);
    Motor* md = bpi->motor(MD);
    s1->setMode(2);
    s2->setMode(1);
    s3->setMode(12);
    s4->setMode(2);

    ma->power(Motor::CW, 255);
    mb->power(Motor::CCW, 255);
    mc->power(Motor::CW, 255);
    md->power(Motor::CW, 255);

    timer.start();

    for (int i=0; i < 1000; i++)
    {
        bpi->update();

        cout << setw(10) << s1->value();
        cout << setw(10) << s2->value();
        cout << setw(10) << s3->value();
        cout << setw(10) << s4->value();

        cout << setw(10) << ma->encoder();
        cout << setw(10) << mb->encoder();
        cout << setw(10) << mc->encoder();
        cout << setw(10) << md->encoder();

        cout << endl;
    }

    cout << "update took " << timer.mElapsed() << "ms" << endl;

    BrickPi::close();

    return EXIT_SUCCESS;
}

