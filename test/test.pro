TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += \
    main.cpp

LIBS += -L$$OUT_PWD/../driver/ -lbrickpi

INCLUDEPATH += $$PWD/../driver
DEPENDPATH += $$PWD/../driver

PRE_TARGETDEPS += $$OUT_PWD/../driver/libbrickpi.a
